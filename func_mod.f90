module func
    use const
    implicit none
contains

    function K(x,t)
        real(mp) :: K
        real(mp) :: x, t

        K = 1.0 / sqrt(const_a - (x+t)) !$ K(x,t) = \frac{1}{\sqrt{a-(x+t)}}$

    end function K

    function f(x)
        real(mp) :: f
        real(mp) :: x

        f = 1.0 / (x + cos(pi*x/2.0)) !$f(x) = \frac{1}{x+\cos{\frac{\pi}{2}x}}$

    end function f

    function jordan_scheme(A,B) result(x)                               ! Метод Гаусса, схема Жордана.
        real(mp) A(:,:), B(:)
        real(mp) x(1:size(B)), aa(1:size(B),1:size(B)+1)
        integer n,i,j,k
        
        n=size(B)
        aa(1:n,1:n)=A
        aa(1:n,n+1)=B
        do k=1,n                                                        ! Грядёт длинное условие проверки ведущего элемента на малость
            if (abs(aa(k,k))<=10_mp**(-2*mp+2)) write(*,*) 'Warning: leading element is close to 0'
            aa(k,:)=aa(k,:)/aa(k,k)
            forall(i=1:n, j=k:n+1, i/=k) aa(i,j)=aa(i,j)-aa(k,j)*aa(i,k)! Матрица полность диагонализуется,
        enddo
        x=aa(:,n+1)                                                     ! в результате решение просто совпадает с пребразованным вектором B.
    end

end module func