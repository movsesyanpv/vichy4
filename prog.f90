use func
use const
use gl

integer :: n = 10
integer :: nn = 50
integer :: i, j
real(mp) :: ufinal

real(mp), allocatable, dimension(:) :: xk, xi, A, u, fi
real(mp), allocatable, dimension(:,:) :: mA

allocate(xk(n),xi(0:nn),A(n),u(n),fi(n),mA(n,n))

call get_xk(n,xk)
call get_w(n,xk,A)

do i = 0, nn
    xi(i) = (hb-lb)/nn * i
enddo

do i =1, n
    xk(i) = xk(i) * (hb-lb)/2.0 + (lb+hb)/2.0
    fi(i) = f(xk(i))
enddo

do i = 1, n
    do j = 1, n
        mA(i,j) = - A(j) * K(xk(i),xk(j))* (hb-lb)/2.0
    enddo   
    mA(i,i) = mA(i,i) + 1
enddo

u = jordan_scheme(mA,fi)

open(10,file = 'res.dat')
do i = 0, nn
    ufinal = f(xi(i))
    do j = 1, n
        ufinal = ufinal + A(j)*K(xi(i),xk(j))*u(j)* (hb-lb)/2.0
    enddo
    write(10,*) xi(i), ufinal
enddo
close(10)
deallocate(xk,xi,A,u,fi,mA)

end